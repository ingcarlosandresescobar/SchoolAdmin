@extends('base')
@section('titulo')
	SchoolAdmin
@endsection

@section('contenido')
	@if (Auth::user()->rol=="estudiante")
       @include('estudiante.index')     
    @elseif (Auth::user()->rol=="docente")
    	@include('docente.index')     
    @elseif (Auth::user()->rol=="admin")
    	@include('admin.index')
    @endif
@endsection