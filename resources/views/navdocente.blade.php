<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ url('/') }}"><i class="fa fa-home fa-fw"></i> Inicio</a>
            </li>
            <li>
                <a href="{{ url('vercursos') }}"><i class="fa fa-table fa-fw"></i> Carga academica</a>
            </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>