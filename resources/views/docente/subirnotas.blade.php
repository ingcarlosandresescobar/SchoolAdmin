@extends('base')
@section('titulo')
    SchoolAdmin
@endsection
@section('contenido')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Subir notas</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">              
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Identificacion</th>
                                        <th>Nombres</th>
                                        <th>Periodo1</th>
                                        <th>Periodo2</th>
                                        <th>Periodo3</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($estudiantes as $row)
                                        <tr class="even gradeC">
                                            {!! Form::open(['action' => 'docente\DocenteController@subirnotas']) !!}
                                            <td>{{ $row->identificacion }} </td>                                           
                                            <td>{{ $row->nombres }} {{ $row->apellidos }} </td>
                                            @if($row->Periodo1 == 0)
                                            <td>{!! Form::input("number", "periodo1", null, ["min" => 0, "max" => 5, "step" => 0.1]) !!}</td>
                                            @else    
                                            <td>{!! Form::input("number", "periodo1", $row->Periodo1, ["min" => 0, "max" => 5, "step" => 0.1]) !!}</td>
                                            @endif
                                            @if($row->Periodo2 == 0)
                                            <td>{!! Form::input("number", "periodo2", null, ["min" => 0, "max" => 5, "step" => 0.1]) !!}</td>
                                            @else    
                                            <td>{!! Form::input("number", "periodo2", $row->Periodo2, ["min" => 0, "max" => 5, "step" => 0.1]) !!}</td>
                                            @endif
                                            @if($row->Periodo3 == 0)
                                            <td>{!! Form::input("number", "periodo3", null, ["min" => 0, "max" => 5, "step" => 0.1]) !!}</td>
                                            @else    
                                            <td>{!! Form::input("number", "periodo3", $row->Periodo3, ["min" => 0, "max" => 5, "step" => 0.1]) !!}</td>
                                            @endif
                                            {!! Form::hidden("estudiante", $row->id)!!}
                                            {!! Form::hidden("asignatura", $asignatura)!!}
                                            <td>{!! Form::submit('Guardar') !!}</td>
                                            {!! Form::close() !!}
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
