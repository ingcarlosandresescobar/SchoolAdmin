@extends('base')
@section('titulo')
    SchoolAdmin
@endsection
@section('contenido')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Asignaturas</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">              
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Grado</th>
                                        <th>Asignatura</th>
                                        <th>Ingresar notas</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($carga as $row)
                                        <tr class="even gradeC">
                                            <td >{{ $row->gradoid }} </td>
                                            <td>{{ $row->nombre }} </td>                                          
                                            <td>                                             
                                                <a href="{{ action('docente\DocenteController@listarestudiantes', $row->id) }}" title="Subir notas"  class="fa fa-search fa-2x"></a>                                               
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection