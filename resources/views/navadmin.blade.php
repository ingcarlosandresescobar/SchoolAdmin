<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
      <li>
        <a href="{{ url('/') }}"><i class="fa fa-home fa-fw"></i>Inicio</a>
      </li>
      <li>
        <a class="fa fa-graduation-cap Estudiantes fa-1x @if (Request::url() == url('/estudiantes')) active @endif " href="#">Estudiantes</a>         
      </li>
      <li>
        <a class="fa fa-user fa-1x @if (Request::url() == action('admin\DocenteController@getdocentes')) active @endif" href="{{ url('/docentes') }}">Docentes</a>                        
      </li>
      <li>
        <a class="fa fa-book fa-1x @if (Request::url() == action('admin\AsignaturaController@getasignaturas')) active @endif" href="{{ url('/asignaturas') }}">Asignaturas</a>
      </li>
    </ul>
  </div>
</div>
