<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ url('/') }}"><i class="fa fa-home fa-fw"></i> Inicio</a>
            </li>
            <li>
                <a href="{{action('estudiante\EstudianteController@vernotas')}}"><i class="fa fa-table fa-fw"></i> Notas</a>
            </li>
        </ul>
    </div>
</div>