@extends('base')
@section('titulo')
  SchoolAdmin
@endsection
@section('contenido')
    <div class="modal fade" id="EditarAsignatura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
        <div class="modal-dialog">
            <h2 style="color: white;" class="page-header">Editar asignatura</h2>
            <div class="modal-content">
                <div class="modal-header">
                    @if (count($errors)>0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hemos encontrado los siguientes errores.<br><br>              
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                    {!! Form::model($asignatura,['method' => 'PATCH', 'action' => ['admin\AsignaturaController@update', $asignatura->id]]) !!}
					<div class="input-group">
					    <div class="input-group-addon">
					        <label class="fa fa-pencil-square-o fa-1x"></label>
					    </div>
					    {!! Form::text('nombre', null, ['placeholder' => 'Nombre', 'class' => 'form-control', 'required']) !!}
					</div>
    				<br>
					<div class="input-group">
					    <div class="input-group-addon">
					        <label class="fa fa-male fa-1x"></label>
					    </div>
					       {!! Form::select('estado', ['' => 'Seleccione el estado', true => 'Activo', false => 'Inactivo'], null, ['class' => 'form-control']) !!}
					</div>
					<br>
                    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
$(window).load(function(){
      $('#EditarAsignatura').modal('show');
      $('#EditarAsignatura').on('hidden.bs.modal', function (e) {
      e.preventDefault();
      window.history.back();
})
  });
@endsection