@extends('base')
@section('titulo')
  SchoolAdmin
@endsection
@section('contenido')
<div class="modal fade" id="Editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
  <h2 style="color: white;" class="page-header">Editar docente</h2>
    <div class="modal-content">
      <div class="modal-header">
             @if (count($errors)>0)
       <div class="alert alert-danger">
         <strong>Whoops!</strong> Hemos encontrado los siguientes errores.<br><br>              
                @foreach ($errors->all() as $error)
                  <p>{{ $error }}</p>
                @endforeach
       </div>
       @endif
       {!! Form::model($docente,['method' => 'PATCH', 'action' => ['admin\DocenteController@update', $docente->id]]) !!}
       @include('formbase')
       {!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
$(window).load(function(){
      $('#Editar').modal('show');
      $('#Editar').on('hidden.bs.modal', function (e) {
      e.preventDefault();
      window.history.back();
})
  });
@endsection
