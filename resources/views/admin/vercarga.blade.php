@extends('base')
@section('titulo')
  SchoolAdmin
@endsection
@section('contenido')	
	<div class="modal fade" id="cargar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	 	<div class="modal-dialog">
	 	  <h2 style="color: white;" align="center">Ver carga academica</h2>
	 	  <h3 style="color: white;" align="center">Docente: {{$docente->nombres}} {{$docente->apellidos}}</h3>
	    	<div class="modal-content">
	      		<div class="modal-header">          
	        		<div class="row">
	                	<div class="col-lg-12">
	                    	<div class="panel panel-default">
	                       		<div class="panel-body">
	                            	<div class="table-responsive">
	                                	<table class="table table-striped">
	                                    	<thead>
	                                        	<tr>
	                                            	<th>Grado</th>
	                                            	<th>Asignatura</th>
	                                            	<th>Eliminar</th>	                                            
	    	                                    </tr>
	        	                            </thead>
		                                    <tbody>
		                                    @if($carga->isempty())
		                                    	<tr>
		                                    	<td align="center">No tiene cursos asignados</td>
		                                    	</tr>
		                                    @else
		                                    @foreach($carga as $row)
		                                        <tr>
		                                            <td>{{$row->gradoid}}</td>
		                                            <td>{{$row->nombre}}</td>	                                           
		                                            <td><a href="{{ action('admin\DocenteController@destroycarga', $row->id)}}" class="fa fa-remove fa-2x"></a></td>	 
		                                        </tr>		                                    
		                                    @endforeach
		                                    @endif
	                                    	</tbody>
	                                	</table>                          
	      							</div>
	    						</div>
	  						</div>
	                	</div>
	                </div>
	            </div>
	      	</div>
	    </div>
	</div>
@endsection
@section('scripts')
$(window).load(function(){
      $('#cargar').modal('show');
      $('#cargar').on('hidden.bs.modal', function (e) {
      e.preventDefault();
      window.history.back();
})
  });
@endsection