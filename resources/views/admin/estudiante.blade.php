@extends('base')
@section('titulo')
	SchoolAdmin
@endsection
@section('contenido')
    @include('admin.agregarestudiante')
	<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Estudiantes</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
           <div class="panel panel-default">
                <div class="panel-heading">                    
                    <button type="button"  data-toggle="modal" data-target="#AgregarEstudiante" class="fa fa-plus fa-1x btn btn-danger"> Agregar</button>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($estudiantes as $row)
                                    <tr class="odd gradeX">
                                        <td>{{ $row->identificacion }}</td>
                                        <td>{{ $row->nombres }}</td>
                                        <td class="center">{{ $row->apellidos}}</td>
                                        <td class="center">
                                            @if ($row->estado == true)
                                                Activo
                                            @else
                                                Inactivo
                                            @endif
                                        </td>
                                        <td class="center">
                                            <table align="center" width="130" border="0">
                                                <tr>
                                                    @if ($row->estado == true)
                                                        <td><a href="{{ action('admin\EstudianteController@destroyestudiante', $row->identificacion) }}" title="Cambiar estado" class="fa fa-remove fa-2x"></a></td>
                                                    @else
                                                        <td><a href="{{ action('admin\EstudianteController@activateestudiante', $row->identificacion) }}" title="Cambiar estado" class="fa fa-check fa-2x"></a></td>
                                                    @endif
                                                    <td><a href="{{ action('admin\EstudianteController@editar', $row->identificacion) }}" title="Editar" class="fa fa-edit fa-2x"></a></td>
                                                    <td><a href="{{ action('admin\EstudianteController@vernotas', $row->identificacion)}}" title="Ver Notas" class="fa fa-search fa-2x"></a></td>
                                                  {{--   <td><a href="{{ action('admin\EstudianteController@verasignaturas', $row->identificacion)}}" title="Ver Asignaturas" class="fa fa-calendar fa-2x"></a></td> --}}
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection