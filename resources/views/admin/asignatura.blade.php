@extends('base')
@section('titulo')
    SchoolAdmin
@endsection
@section('contenido') 
    @include('admin.agregarasignatura')   
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Asignaturas</h1>
        </div>
    </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> 
                    <button type="button" data-toggle="modal" data-target="#AgregarAsignatura" class="fa fa-plus fa-1x btn btn-danger"> Agregar</button>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                      <th>ID</th>
                                      <th>Nombres</th>
                                      <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($asignaturas as $row)
                                    <tr class="odd gradeX">
                                    {!! Form::open(['action' => ('admin\AsignaturaController@agregarasignatura')]) !!}
                                      <td>{{$row->id}}</td>
                                      <td>{{$row->nombre}}</td>
                                      <td class="center">
                                            @if ($row->estado == true)
                                                Activo
                                            @else
                                                Inactivo
                                            @endif
                                      </td>
                                        <td class="center">
                                                    @if ($row->estado == true)
                                                    <a href="{{ action('admin\AsignaturaController@destroyasignatura', $row->id) }}" title="Cambiar estado"   class="fa fa-remove fa-2x">&nbsp</a>
                                                @else
                                                    <a href="{{ action('admin\AsignaturaController@activateasignatura', $row->id) }}" title="Cambiar estado"   class="fa fa-check fa-2x">&nbsp</a>
                                                @endif
                                                 <a href="{{ action('admin\AsignaturaController@editar', $row->id)}}" title="Editar"  class="fa fa-edit fa-2x">&nbsp</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection