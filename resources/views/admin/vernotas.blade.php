@extends('base')
@section('titulo')
  SchoolAdmin
@endsection
@section('contenido')
    <div class="modal fade" id="VerNotas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <h2 style="color: white;" align="center"> Calificaciones</h2>
            <h3 style="color: white;" align="center"> Estudiante: {{ $estudiantes->nombres }} {{$estudiantes->apellidos}}</h3>
            <div class="modal-content">
                <div class="modal-header">               
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Asignatura</th>
                                                    <th>Periodo 1</th>
                                                    <th>Periodo 2</th>
                                                    <th>Periodo 3</th>
                                                    <th>Nota Final</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($notas as $nota)
                                                    <tr>
                                                        <td>{{$nota->nombre}}</td>
                                                        @if($nota->Periodo1 == 0)
                                                            <td></td>
                                                        @else
                                                            <td>{{$nota->Periodo1}}</td>
                                                        @endif
                                                        @if($nota->Periodo2 == 0)
                                                            <td></td>
                                                        @else
                                                            <td>{{$nota->Periodo2}}</td>
                                                        @endif
                                                        @if($nota->Periodo3 == 0)
                                                            <td></td>
                                                        @else
                                                            <td>{{$nota->Periodo3}}</td>
                                                        @endif
                                                        @if (($nota->Periodo1 == 0) || ($nota->Periodo2 == 0) || ($nota->Periodo3 == 0))
                                                            <td></td>
                                                        @else
                                                            <td>{{($nota->Periodo1+$nota->Periodo2+$nota->Periodo3)/3}}</td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
$(window).load(function(){
      $('#VerNotas').modal('show');
      $('#VerNotas').on('hidden.bs.modal', function (e) {
      e.preventDefault();
      window.history.back();
})
  });
@endsection