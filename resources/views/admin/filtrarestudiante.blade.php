<div class="modal fade" id="EstudianteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
  <div class="modal-dialog">
   <h2 style="color: white;" class="page-header">Filtrar estudiantes</h2>
    <div class="modal-content">
      <div class="modal-header">
             @if (count($errors)>0)
       <div class="alert alert-danger">
         <strong>Whoops!</strong> Hemos encontrado los siguientes errores.<br><br>              
                @foreach ($errors->all() as $error)
                  <p>{{ $error }}</p>
                @endforeach
       </div>
       @endif
        {!! Form::open(['method' => 'POST', 'action' => 'admin\EstudianteController@filtrarestudiantes']) !!}
               <div class="input-group">
    <div class="input-group-addon">
        <label class="fa fa-info-circle fa-1x"></label>
    </div>
       {!! Form::select('grado', ['' => 'Seleccione un grado', '1' => 1, '2' => 2, '3' => 3,'4' => 4,'5' => 5,'6'=>6, '7'=>7,'8'=>8,'9'=>9,'10'=>10,'11' => 11], null, ['class' => 'form-control','required']) !!}
       </div>
       <br>
        <div class="input-group">
       <div class="input-group-addon">
        <label class="fa fa-info-circle fa-1x"></label>
    </div>
       {!! Form::select('estado', ['' => 'Seleccione un estado', 1 => 'Activo', 0 => 'Inactivo', 'todos' => 'Todos'], null, ['class' => 'form-control','required']) !!}
       </div>   
        <br>
        {!! Form::submit('Buscar', ['id' => 'buscar', 'class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
<br>
  </div>
</div>
</div>
</div>