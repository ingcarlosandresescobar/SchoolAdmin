@extends('base')
@section('titulo')
    SchoolAdmin
@endsection
@section('contenido')
    @include('admin.agregardocente')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Docentes</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> 
                    <button type="button"  data-toggle="modal" data-target="#AgregarDocente" class="fa fa-plus fa-1x btn btn-danger"> Agregar</button>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($docentes as $row)
                                        <tr class="even gradeC">
                                            <td>{{ $row->identificacion }} </td>
                                            <td>{{ $row->nombres }} </td>
                                            <td class="center">{{ $row->apellidos }}</td>
                                            <td class="center">
                                                @if ($row->estado == true)
                                                    Activo
                                                @else
                                                    Inactivo
                                                @endif
                                            </td>
                                            <td class="center" align="center">
                                                @if ($row->estado == true)
                                                    <a href="{{ action('admin\DocenteController@destroydocente', $row->identificacion) }}" title="Cambiar estado"   class="fa fa-remove fa-2x">&nbsp</a>
                                                @else
                                                    <a href="{{ action('admin\DocenteController@activatedocente', $row->identificacion) }}" title="Cambiar estado"   class="fa fa-check fa-2x">&nbsp</a>
                                                @endif
                                                <a href="{{ action('admin\DocenteController@editar', $row->identificacion) }}" title="Editar"  class="fa fa-edit fa-2x">&nbsp</a>
                                                <a href="{{ action('admin\DocenteController@asignarcarga', $row->identificacion) }}" title="Asignar carga" class="fa fa-users fa-2x">&nbsp</a>
                                                <a href="{{ action('admin\DocenteController@vercarga', $row->identificacion)}}" title="Ver carga" class="fa fa-calendar fa-2x"></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection