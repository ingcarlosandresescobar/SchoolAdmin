@extends('base')
@section('titulo')
  SchoolAdmin
@endsection
@section('contenido')
<div class="modal fade" id="Editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
  <h2 style="color: white;" class="page-header">Editar estudiante</h2>
    <div class="modal-content">
      <div class="modal-header">
             @if (count($errors)>0)
       <div class="alert alert-danger">
         <strong>Whoops!</strong> Hemos encontrado los siguientes errores.<br><br>              
                @foreach ($errors->all() as $error)
                  <p>{{ $error }}</p>
                @endforeach
       </div>
       @endif
       {!! Form::model($estudiante,['method' => 'PATCH', 'action' => ['admin\EstudianteController@update', $estudiante->id]]) !!}
       @include('formbase')
       <div class="input-group">
    <div class="input-group-addon">
        <label class="fa fa-info-circle fa-1x"></label>
    </div>
       {!! Form::select('grado', ['' => 'Seleccione un grado', '1' => 1, '2' => 2, '3' => 3,'4' => 4,'5' => 5,'6'=>6, '7'=>7,'8'=>8,'9'=>9,'10'=>10,'11' => 11], null, ['class' => 'form-control']) !!}
       </div> 
       <br>
       {!! Form::submit('Actualizar', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
$(window).load(function(){
      $('#Editar').modal('show');
      $('#Editar').on('hidden.bs.modal', function (e) {
      e.preventDefault();
      window.history.back();
})
  });
@endsection
