    <div class="modal fade" id="AgregarAsignatura" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
        <div class="modal-dialog">
            <h2 style="color: white;" class="page-header">Agregar asignatura</h2>
            <div class="modal-content">
                <div class="modal-header">
                    @if (count($errors)>0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hemos encontrado los siguientes errores.<br><br>              
                            @foreach ($errors->all() as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                    {!! Form::open(['action' => 'admin\AsignaturaController@agregarasignatura']) !!}
					<div class="input-group">
					    <div class="input-group-addon">
					        <label class="fa fa-pencil-square-o fa-1x"></label>
					    </div>
					    {!! Form::text('nombre', null, ['placeholder' => 'Nombre', 'class' => 'form-control', 'required']) !!}
					</div>
    				<br>
					<div class="input-group">
					    <div class="input-group-addon">
					        <label class="fa fa-male fa-1x"></label>
					    </div>
					       {!! Form::select('estado', ['' => 'Seleccione el estado', true => 'Activo', false => 'Inactivo'], null, ['class' => 'form-control']) !!}
					</div>
					<br>
                    {!! Form::submit('Registrar', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
