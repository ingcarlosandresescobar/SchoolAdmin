<div class="modal fade" id="AgregarDocente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
  <h2 style="color: white;" class="page-header">Agregar docente</h2>
    <div class="modal-content">
      <div class="modal-header">
             @if (count($errors)>0)
       <div class="alert alert-danger">
         <strong>Whoops!</strong> Hemos encontrado los siguientes errores.<br><br>              
                @foreach ($errors->all() as $error)
                  <p>{{ $error }}</p>
                @endforeach
       </div>
       @endif
       {!! Form::open(['action' => 'admin\DocenteController@agregardocente']) !!}
       @include('formbase')
       {!! Form::submit('Registrar', ['style' => 'float: left' ,'class' => 'btn btn-primary']) !!}
       <br>
       <br>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>