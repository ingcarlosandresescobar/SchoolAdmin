@extends('base')
@section('titulo')
  SchoolAdmin
@endsection
@section('contenido')	
	<div class="modal fade" id="cargar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	 	<div class="modal-dialog">
	 	  <h2 style="color: white;" align="center">Ver asignaturas</h2>
	 	  <h3 style="color: white;" align="center">Estudiante: {{$estudiante->nombres}} {{$estudiante->apellidos}}</h3>
	    	<div class="modal-content">
	      		<div class="modal-header">          
	        		<div class="row">
	                	<div class="col-lg-12">
	                    	<div class="panel panel-default">
	                       		<div class="panel-body">
	                            	<div class="table-responsive">
	                                	<table class="table table-striped">
	                                    	<thead>
	                                        	<tr>
	                                            	<th>Asignatura</th>
	    	                                    </tr>
	        	                            </thead>
		                                    <tbody>
		                                    @if($asignaturas->isempty())
		                                    	<tr>
		                                    	<td align="center">No tiene cursos asignados</td>
		                                    	</tr>
		                                    @else
		                                    @foreach($asignaturas as $row)
		                                        <tr>
		                                            <td>{{$row->nombre}}</td>	                                           
		                                        </tr>		                                    
		                                    @endforeach
		                                    @endif
	                                    	</tbody>
	                                	</table>                          
	      							</div>
	    						</div>
	  						</div>
	                	</div>
	                </div>
	            </div>
	      	</div>
	    </div>
	</div>
@endsection
@section('scripts')
$(window).load(function(){
      $('#cargar').modal('show');
      $('#cargar').on('hidden.bs.modal', function (e) {
      e.preventDefault();
      window.history.back();
})
  });
@endsection