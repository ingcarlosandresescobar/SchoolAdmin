<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Panel de Administración</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-graduation-cap fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">&nbsp;</div>
                                    <div>Gestión de Estudiantes</div>
                                </div>
                            </div>
                        </div>
                        <a class="Estudiantes" href="#">
                            <div class="panel-footer">
                                <span class="pull-left">Detalles</span></br>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">&nbsp;</div>
                                    <div>Gestión de Docentes</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{url('/docentes')}}">
                            <div class="panel-footer">
                                <span class="pull-left">Detalles</span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-book fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">&nbsp;</div>
                                    <div>Gestión de Asignaturas</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{ url('/asignaturas') }}">
                            <div class="panel-footer">
                                <span class="pull-left">Detalles</span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                
            </div>