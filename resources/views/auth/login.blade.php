@extends('auth.authbase')
@section('titulo')
	Ingresar
	@endsection
@section('contenido')
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ url('/') }}">SchoolAdmin</a>
            </div>
            <!-- /.navbar-header -->
</nav>
<div class="container">
<div class="container">
    <div class="row colored">
        <div class="contcustom">
            <img src="{{ asset('img/logo.png') }}" alt="">
            <h2>Ingreso</h2>
             @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Whoops!</strong> Hemos encontrado los siguientes errores.<br><br>              
                @foreach ($errors->all() as $error)
                  <p>{{ $error }}</p>
                @endforeach
              </ul>
            </div>
          @endif
            <div>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/ingresar') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <table width="auto" align="center">
  <tr>
    <td><div class="fa fa-user logo" ></div> </td>
    <td><input type="text" name="identificacion" placeholder="Identificacion" required></td>
  </tr>
  <tr>
    <td><div class="fa fa-lock logo" ></div></td>
    <td><input type="password" name="password" placeholder="Contraseña" required></td>
  </tr>
  <tr>
    <td colspan="2">
    <a href="index.html"><button type="submit" class="btn btn-default wide"><span class="fa fa-book fa-2x biglogo"></span></button></a>
    </td>
  </tr>
</table>
</form>
              
            </div>
        </div>
    </div>

</div>
@endsection
@stop