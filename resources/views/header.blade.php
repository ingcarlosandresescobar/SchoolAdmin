<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">SchoolAdmin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ Auth::user()->nombres }} {{ Auth::user()->apellidos }} 
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>                    
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ url('/salir') }}"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            @if (Auth::user()->rol=="admin")
            	@include('navadmin')
            @elseif (Auth::user()->rol=="estudiante")
            	@include('navestudiante')
            @elseif (Auth::user()->rol=="docente")
                @include('navdocente')
            @else
            	Error, no se reconoce el rol
            @endif
        </nav>