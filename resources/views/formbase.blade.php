<div class="input-group">
    <div class="input-group-addon">
        <label class="fa fa-user fa-1x"></label>
    </div>
        {!! Form::text('identificacion', null, ['placeholder' => 'Identificación', 'class' => 'form-control', 'pattern' => '[0-9]+', 'required']) !!}
</div>
<br>
<div class="input-group">
    <div class="input-group-addon">
        <label class="fa fa-pencil-square-o fa-1x"></label>
    </div>
    {!! Form::text('nombres', null, ['placeholder' => 'Nombres', 'class' => 'form-control', 'required']) !!}
</div>
    <br>
<div class="input-group">
    <div class="input-group-addon">
        <label class="fa fa-pencil-square-o fa-1x"></label>
    </div>
        {!! Form::text('apellidos', null, ['placeholder' => 'Apellidos', 'class' => 'form-control', 'required']) !!}
</div>
<br>
<div class="input-group">
    <div class="input-group-addon">
        <label class="fa fa-at fa-1x"></label>
    </div>
        {!! Form::email('email', null, ['placeholder' => 'Correo', 'class' => 'form-control', 'required']) !!}
</div>
<br>
<div class="input-group">
    <div class="input-group-addon">
        <label class="fa fa-key fa-1x"></label>
    </div>
    {!! Form::input('password', 'password', null, ['placeholder' => 'Contraseña', 'class' => 'form-control', 'required'])!!}
</div>
<br>
<div class="input-group">
    <div class="input-group-addon">
        <label class="fa fa-calendar-o fa-1x"></label>
    </div>
        {!! Form::input('date', 'fnacimiento', null, ['placeholder' => 'Fecha de nacimiento', 'class' => 'form-control', 'required']) !!}
</div>
<br>
<div class="input-group">
    <div class="input-group-addon">
        <label class="fa fa-male fa-1x"></label>
    </div>
       {!! Form::select('sexo', ['' => 'Seleccione el genero', 'masculino' => 'Masculino', 'femenino' => 'Femenino'], null, ['class' => 'form-control']) !!}
</div>
<br>