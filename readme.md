## SchoolAdmin

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/downloads.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

SchoolAdmin, proyecto de Ingeniería del Software, panel de gestión de una escuela.

## Configuración del entorno de desarrollo

Inicialmente se requiere tener PHP5 con varias extensiones instaladas:

sudo apt-get install mysql-server php5-cli php5-mysql php5-gd php5-mcrypt

Habilitamos la extensión:

sudo php5enmod mcrypt


Tambien se requiere crear una base de datos donde se guardaran las tablas de nuestra aplicación Laravel.


### Instalamos composer:


cd ~ 

curl -sS https://getcomposer.org/installer | php 

sudo mv composer.phar /usr/local/bin/composer 



### Clonamos el repositorio del proyecto SchoolAdmin:

git clone https://github.com/MayteHerrera/SchoolAdmin.git 

cd SchoolAdmin/ && composer install && composer update


### Renombramos el .env.ejemplo a .env y lo editamos con nuestros datos de conexión a nuestro servidor de base de datos:

mv .env.ejemplo .env 


### Configuramos una nueva llave de aplicación:

php artisan key:generate

### Una vez tengamos nuestro entorno configurado procedemos a migrar la base de datos:

php artisan migrate:install 

php artisan migrate 

### Ahora generamos los datos de ejemplo para la tabla de usuarios:

php artisan db:seed 


### Finalmente corremos el servidor web de Laravel:
php artisan serve 

Ingresamos a nuestro navegador por la siguiente url: 

http://localhost:8000



## Contribuidores

Maria Teresa Herrera 

Ronald Alfonso Fuentes