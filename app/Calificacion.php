<?php namespace SchoolAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\hasOne;

class Calificacion extends Model {

	protected $table = 'calificaciones';
	public $timestamps = false;
	protected $fillable = ['asignatura_grados_id','estudiante_id', 'Periodo1', 'Periodo2', 'Periodo3'];

}
