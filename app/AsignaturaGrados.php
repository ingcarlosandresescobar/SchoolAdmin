<?php namespace SchoolAdmin;

use Illuminate\Database\Eloquent\Model;

class AsignaturaGrados extends Model {

	protected $table = 'asignatura_grados';

	public $timestamps = false;

	protected $fillable = ['asignatura_id','docente_id', 'gradoid'];



}
