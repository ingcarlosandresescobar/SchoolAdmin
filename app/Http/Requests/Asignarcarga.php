<?php namespace SchoolAdmin\Http\Requests;

use SchoolAdmin\Http\Requests\Request;

class Asignarcarga extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'grado' => 'required',
			'asignatura' => 'required',
		];
	}

}
