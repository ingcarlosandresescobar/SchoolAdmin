<?php namespace SchoolAdmin\Http\Requests;

use SchoolAdmin\Http\Requests\Request;

class AddEstudiante extends Request {

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'nombres' => 'required',
			'apellidos' => 'required',
			'identificacion' => 'required|max:10',
			'email' => 'required|email',
			'grado' => 'required',
			'sexo' => 'required',
			'fnacimiento' => 'required',
		];
	}

}
