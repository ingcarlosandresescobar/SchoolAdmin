<?php namespace SchoolAdmin\Http\Requests;

use SchoolAdmin\Http\Requests\Request;

class AddDocente extends Request {

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'nombres' => 'required',
			'apellidos' => 'required',
			'identificacion' => 'required|max:10',
			'email' => 'required|email',
			'sexo' => 'required',
			'fnacimiento' => 'required',
		];
	}

}
