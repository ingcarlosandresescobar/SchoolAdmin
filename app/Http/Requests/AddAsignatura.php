<?php namespace SchoolAdmin\Http\Requests;

use SchoolAdmin\Http\Requests\Request;

class AddAsignatura extends Request {

	public function authorize()
	{
		return true;
	}

	public function rules()
	{
		return [
			'nombre' => 'required',
			'estado' => 'required',
		];
	}

}
