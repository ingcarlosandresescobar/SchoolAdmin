<?php


Route::group(['middleware' => 'SchoolAdmin\Http\Middleware\AdminMiddleware'], function(){
	//RUTAS ADMIN ESTUDIANTE
	Route::post('/estudiantes', 'admin\EstudianteController@filtrarestudiantes');
	Route::get('/estudiantes', 'admin\EstudianteController@getestudiantes');
	Route::get('/estudiante/{id}/eliminar', 'admin\EstudianteController@destroyestudiante');
	Route::get('/estudiante/{id}/activar', 'admin\EstudianteController@activateestudiante');
	Route::post('/estudiante/registrar', 'admin\EstudianteController@agregarestudiante');
	Route::get('/estudiante/{id}/editar', 'admin\EstudianteController@editar');
	Route::get('/estudiante/{id}/notas', 'admin\EstudianteController@vernotas');
	Route::patch('/estudiante/{id}/update', 'admin\EstudianteController@update');
	Route::get('/estudiante/{id}/asignaturas', 'admin\EstudianteController@verasignaturas');

	//RUTAS ADMIN DOCENTE
	Route::get('/docentes', 'admin\DocenteController@getdocentes');
	Route::get('/docente/{id}/eliminar', 'admin\DocenteController@destroydocente');
	Route::get('/docente/{id}/activar', 'admin\DocenteController@activatedocente');
	Route::post('/docente/registrar', 'admin\DocenteController@agregardocente');
	Route::get('/docente/{id}/editar', 'admin\DocenteController@editar');
	Route::get('/docente/{id}/asignar', 'admin\DocenteController@asignarcarga');
	Route::patch('/docente/{id}/asignar', 'admin\DocenteController@postasignar');
	Route::patch('/docente/{id}/update', 'admin\DocenteController@update');
	Route::get('/docente/{id}/carga', 'admin\DocenteController@vercarga');
	Route::get('/docente/{id}/carga/eliminar', 'admin\DocenteController@destroycarga');

	//RUTAS ADMIN ASIGNATURA
	Route::get('/asignaturas', 'admin\AsignaturaController@getasignaturas');
	Route::post('/asignatura/agregar', 'admin\AsignaturaController@agregarasignatura');
	Route::get('/asignatura/{id}/eliminar', 'admin\AsignaturaController@destroyasignatura');
	Route::get('/asignatura/{id}/activar', 'admin\AsignaturaController@activateasignatura');
	Route::get('/asignatura/{id}/editar', 'admin\AsignaturaController@editar');
	Route::patch('/asignatura/{id}/update', 'admin\AsignaturaController@update');
});


//RUTAS ESTUDIANTES
Route::group(['middleware' => 'SchoolAdmin\Http\Middleware\EstudianteMiddleware'], function(){
	Route::get('/vernotas', 'estudiante\EstudianteController@vernotas');
});

//RUTAS DOCENTES
Route::group(['middleware' => 'SchoolAdmin\Http\Middleware\DocenteMiddleware'], function(){
	Route::get('/vercursos', 'docente\DocenteController@vercursos');
	Route::get('/listarestudiantes/{id}', 'docente\DocenteController@listarestudiantes');
	Route::post('/subirnotas', 'docente\DocenteController@subirnotas');
});
//RUTAS GENERALES
Route::get('/', 'IndexController@index');
Route::get('ingresar', 'Auth\AuthController@getLogin');
Route::post('ingresar', 'Auth\AuthController@postLogin');
Route::get('salir', 'Auth\AuthController@getLogout');
Route::get('recuperar', 'Auth\PasswordController@getEmail');	
Route::post('recuperar', 'Auth\PasswordController@postEmail');
Route::get('recuperar/reset', 'Auth\PasswordController@getReset');
Route::post('recuperar/reset', 'Auth\PasswordController@postReset');