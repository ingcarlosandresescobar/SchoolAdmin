<?php namespace SchoolAdmin\Http\Controllers;

class IndexController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	
	public function index()
	{
		return view('index');
	}

}
