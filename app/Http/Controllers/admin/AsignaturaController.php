<?php namespace SchoolAdmin\Http\Controllers\admin;

use SchoolAdmin\Http\Requests;
use SchoolAdmin\Http\Controllers\Controller;
use SchoolAdmin\Asignatura;
use SchoolAdmin\Http\Requests\AddAsignatura;
use Illuminate\Http\Request;

class AsignaturaController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}
  #Funcion para filtar por estado las asignaturas
	function getasignaturas(){
		$asignaturas = Asignatura::orderBy('estado','DESC')->get();
		return view('admin.asignatura',compact('asignaturas'));
	}
#Funcion para detruir asignaturas
	function destroyasignatura($id){
		$destroy = Asignatura::where('id',$id)->update(array('estado' => false));

		return redirect('/asignaturas');
	}
#Funcion para activar asignaturas
	function activateasignatura($id){
		$destroy = Asignatura::where('id',$id)->update(array('estado' => true));

	return redirect('/asignaturas');
	}
#Funcion para agregar asignaturas
	public function agregarasignatura(AddAsignatura $request){
		$nombre = $request->input('nombre');
		$estado = $request->input('estado');
		$asignatura = Asignatura::create([
			'nombre' => $nombre,
			'estado' => $estado
		]);
		return redirect('/asignaturas');
	}
#Funcion para editar asignaturas
	public function editar($id)
	{
		$asignatura = Asignatura::findorFail($id);
		return view('admin.editarasignatura', compact('asignatura'));
	}

#Funcion para actualizar asignaturas
	public function update($id, AddAsignatura $request){
		$asignatura = Asignatura::findorFail($id);
		$nombre = $request->input('nombre');
		$estado = $request->input('estado');
		$asignatura->update([
			'nombre' => $nombre,
			'estado' => $estado,
			]);
	return redirect('/asignaturas');
	}

}
