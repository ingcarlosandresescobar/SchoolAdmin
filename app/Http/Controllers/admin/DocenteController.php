<?php namespace SchoolAdmin\Http\Controllers\admin;

use SchoolAdmin\Http\Requests;
use SchoolAdmin\Http\Controllers\Controller;
use SchoolAdmin\User;
use SchoolAdmin\Docente;
use SchoolAdmin\Asignatura;
use SchoolAdmin\AsignaturaGrados;
use Illuminate\Http\Request;
use SchoolAdmin\Http\Requests\AddDocente;
use SchoolAdmin\Http\Requests\Asignarcarga;
use Illuminate\Database\Query\Builder;
class DocenteController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}
#Funcion para agregar Docente
	function agregardocente(AddDocente $request){
		$nombres = $request->input('nombres');
		$apellidos = $request->input('apellidos');
		$identificacion = $request->input('identificacion');
		$email = $request->input('email');
		$password = bcrypt($request->input('password'));
		$rol = 'docente';
		$fnacimiento = $request->input('fnacimiento');
		$sexo = $request->input('sexo');
		if ($sexo == "2"){
			$sexo = 'femenino';
		}
		elseif ($sexo == "1") {
			$sexo = 'masculino';
		}

		$info = array(
			'nombres' => $nombres,
			'apellidos' => $apellidos,
			'identificacion' => $identificacion,
			'email' => $email,
			'password' => $password,
			'rol' => $rol,
			'fnacimiento' => $fnacimiento,
			'sexo' => $sexo,
		);

		User::create($info);
		$id = User::where('identificacion',$identificacion)->get();
		$id->toArray();
		Docente::create([
			'user_id' => $id[0]['id']			
		]);

		return redirect('/docentes');
	}
#Funcion para mostrar los docentes
	function getdocentes(){
		$docentes = User::join('docentes','users.id', '=', 'docentes.user_id')->where('rol','docente')->get();
		return view('admin.docente',compact('docentes'));
	}
#Funcion para Editar Docente
	public function editar($id)
	{
		$docente = User::where('identificacion',$id)->first();
		return view('admin.editardocente', compact('docente'));
	}
#Funcion para asignar carga academica a Docente
	public function asignarcarga($id)
	{
		//$docente = User::where('identificacion', $id)->first();
		$docente = User::join('docentes','users.id', '=', 'docentes.user_id')->where('rol','docente')->where('identificacion',$id)->first();
		$asignatura = Asignatura::where('estado', 1)->get();
		$asignaturas = array();
		foreach ($asignatura as $key) {
			$asignaturas[$key->id] = $key->nombre;
		}
		return view('admin.carga', ['docente' => $docente, 'asignatura' => $asignaturas]);
	}

	public function postasignar($id, Asignarcarga $request){
		$docente = Docente::findOrFail($id);
		$grado = $request->input('grado');
		$asignatura = $request->input('asignatura');

		$info = array(
			'docente_id' => $id,
			'gradoid' => $grado,
			'asignatura_id' => $asignatura
			);
		AsignaturaGrados::create($info);
		return redirect('/docentes');
	}
#Funcion para actualizar  Docente
	public function update($id, AddDocente $request)
	{
		$docente = User::findOrFail($id);
		$nombres = $request->input('nombres');
		$apellidos = $request->input('apellidos');
		$identificacion = $request->input('identificacion');
		$email = $request->input('email');
		$password = bcrypt($request->input('password'));
		$rol = 'docente';
		$fnacimiento = $request->input('fnacimiento');
		$sexo = $request->input('sexo');
		if ($sexo == "2"){
			$sexo = 'femenino';
		}
		elseif ($sexo == "1") {
			$sexo = 'masculino';
		}

		$info = array(
			'nombres' => $nombres,
			'apellidos' => $apellidos,
			'identificacion' => $identificacion,
			'email' => $email,
			'password' => $password,
			'rol' => $rol,
			'fnacimiento' => $fnacimiento,
			'sexo' => $sexo,
		);

		$docente->update($info);
		return redirect('/docentes');
	}
#Funcion para inactivar  Docente
	function destroydocente($id){
		$destroy = User::where('identificacion',$id)->update(array('estado' => false));
		return redirect('/docentes');
	}
#Funcion para activar Docente
	function activatedocente($id){
		$activate = User::where('identificacion',$id)->update(array('estado' => true));
		return redirect('/docentes');
	}
#Funcion para ver carga a Docente
	function vercarga($id){
		$docentes = User::join('docentes','users.id', '=', 'docentes.user_id')->where('rol','docente')->where('identificacion',$id)->first();
		$docente_id = $docentes->id;
		$carga = AsignaturaGrados::join('asignaturas', 'asignatura_grados.asignatura_id', '=', 'asignaturas.id')->where('docente_id', $docente_id)->get();
		return view('admin.vercarga', ['carga' => $carga, 'docente' => $docentes]);
	}
#Funcion para quitar carga a Docente
	function destroycarga($id){
		$destroy = AsignaturaGrados::where('asignatura_id', $id);
		$destroy->delete();
		return redirect('/docentes');
	}

}
