<?php namespace SchoolAdmin\Http\Controllers\admin;

use SchoolAdmin\Http\Requests;
use SchoolAdmin\Http\Controllers\Controller;
use SchoolAdmin\User;
use SchoolAdmin\Estudiante;
use SchoolAdmin\Asignatura;
use SchoolAdmin\Calificacion;
use SchoolAdmin\AsignaturaGrados;
use Illuminate\Http\Request;
use SchoolAdmin\Http\Requests\AddEstudiante;
use SchoolAdmin\Http\Requests\FiltrarEstudiantes;
use Illuminate\Database\Query\Builder;

class EstudianteController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}
#Funcion para agregar Estudiante
	public function agregarestudiante(AddEstudiante $request){
		$nombres = $request->input('nombres');
		$apellidos = $request->input('apellidos');
		$identificacion = $request->input('identificacion');
		$email = $request->input('email');
		$password = bcrypt($request->input('password'));
		$rol = 'estudiante';
		$fnacimiento = $request->input('fnacimiento');
		$grado = $request->input('grado');
		$sexo = $request->input('sexo');
		if ($sexo == "2"){
			$sexo = 'femenino';
		}
		elseif ($sexo == "1") {
			$sexo = 'masculino';
		}

		$info = array(
			'nombres' => $nombres,
			'apellidos' => $apellidos,
			'identificacion' => $identificacion,
			'email' => $email,
			'password' => $password,
			'rol' => $rol,
			'fnacimiento' => $fnacimiento,
			'sexo' => $sexo,
		);

		User::create($info);
		$id = User::where('identificacion',$identificacion)->get();
		$id->toArray();
		Estudiante::create([
			'user_id' => $id[0]['id'],
			'grado' => $grado
		]);

		return redirect('/estudiantes');
	}
	#Funcion para filtar  Estudiante
	public function filtrarestudiantes(FiltrarEstudiantes $request){
		$grado = $request->input('grado');
		$estado = $request->input('estado');
		if($estado == "todos")
		{
			$estudiantes = Estudiante::join('users','estudiantes.user_id', '=', 'users.id')->where('grado',$grado)->get();	
			return view('admin.estudiante',compact('estudiantes'));
		}
		$estudiante = Estudiante::join('users','estudiantes.user_id', '=', 'users.id')->where('grado',$grado)->get();
		$estudiantes = $estudiante->where('estado',$estado);
		return view('admin.estudiante',compact('estudiantes'));
	}
#Funcion para listar Estudiante
	public function getestudiantes(){
		$estudiantes = User::join('estudiantes','users.id', '=', 'estudiantes.user_id')->where('rol','estudiante')->get();
		return view('admin.estudiante',compact('estudiantes'));
	}
#Funcion para editar Estudiante
	public function editar($id)
	{
		$estudiante = User::where('identificacion', $id)->first();
		return view('admin.editarestudiante', ['estudiante'=>$estudiante]);
	}
#Funcion para actualizar Estudiante
	public function update($id, AddEstudiante $request)
	{
		$estudiante = User::findOrFail($id);
		$identificacion = $request->input('identificacion');
		$nombres = $request->input('nombres');
		$apellidos = $request->input('apellidos');
		$email = $request->input('email');
		$password = bcrypt($request->input('password'));
		$rol = 'estudiante';
		$fnacimiento = $request->input('fnacimiento');
		$grado = $request->input('grado');
		$sexo = $request->input('sexo');
		if ($sexo == "2"){
			$sexo = 'femenino';
		}
		elseif ($sexo == "1") {
			$sexo = 'masculino';
		}

		$info = array(
			'nombres' => $nombres,
			'apellidos' => $apellidos,
			'identificacion' => $identificacion,
			'email' => $email,
			'password' => $password,
			'rol' => $rol,
			'fnacimiento' => $fnacimiento,
			'sexo' => $sexo,
		);

		$affectedRows = Estudiante::where('user_id', $id)->update(['grado' => $grado]);

		$estudiante->update($info);
		return redirect('/estudiantes');
	}
#Funcion para inactivar Estudiante
	public function destroyestudiante($id){
		$destroy = User::where('identificacion',$id)->update(array('estado' => false));
		return redirect('/estudiantes');
	}
#Funcion para activar Estudiante
	public function activateestudiante($id){
		$activate = User::where('identificacion',$id)->update(array('estado' => true));
		return redirect('/estudiantes');
	}

	public function vernotas($id){
		$estudiantes = User::join('estudiantes','users.id', '=', 'estudiantes.user_id')->where('rol','estudiante')->where('identificacion',$id)->first();
		$estudiante_id = $estudiantes->id;
		$estudiante_grado = $estudiantes->grado;
		$notas = Calificacion::join('asignatura_grados','calificaciones.asignatura_grados_id','=', 'asignatura_grados.id')->join('asignaturas', 'asignatura_grados.asignatura_id', '=', 'asignaturas.id')->get();
		$nota = $notas->where('estudiante_id', $estudiante_id);
		if ($nota->isempty())
		{
			$nota = AsignaturaGrados::join('asignaturas', 'asignatura_grados.asignatura_id', '=', 'asignaturas.id')->where('gradoid', $estudiante_grado)->get();	
		}
		return view('admin.vernotas', ['notas' => $nota, 'estudiantes' => $estudiantes]);
	}

	public function verasignaturas($id){
		$estudiantes = User::join('estudiantes','users.id', '=', 'estudiantes.user_id')->where('rol','estudiante')->where('identificacion',$id)->first();
		$estudiante_grado = $estudiantes->grado;
		$asignaturas = AsignaturaGrados::join('asignaturas', 'asignatura_grados.asignatura_id', '=', 'asignaturas.id')->where('grado', $estudiante_grado)->get();
		return view('admin.verasignaturas', ['asignaturas' => $asignaturas, 'estudiante' => $estudiantes]);
	}
}