<?php namespace SchoolAdmin\Http\Controllers\docente;

use SchoolAdmin\Http\Requests;
use SchoolAdmin\Http\Requests\NotaRequest;
use SchoolAdmin\Http\Controllers\Controller;
use SchoolAdmin\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use SchoolAdmin\Calificacion;
use SchoolAdmin\AsignaturaGrados;
use SchoolAdmin\Asignatura;

class DocenteController extends Controller {
	protected $auth;

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}
#Funcion para ver cursos 
	public function vercursos(){
		$id = $this->auth->user()->id;
		$docente = User::findorfail($id);
		$identificacion = $this->auth->user()->identificacion;
		$docentes = User::join('docentes','users.id', '=', 'docentes.user_id')->where('rol','docente')->where('identificacion',$identificacion)->first();
		$docente_id = $docentes->id;
		$carga = Asignatura::join('asignatura_grados', 'asignaturas.id', '=', 'asignatura_grados.asignatura_id')->where('docente_id', $docente_id)->get();
		return view('docente.vercursos', ['carga' => $carga, 'docente' => $docentes]);
	}
	#Funcion para listar Estudiantes

	public function listarestudiantes($id){
		$estudiantes = AsignaturaGrados::join('estudiantes', 'asignatura_grados.gradoid', '=', 'estudiantes.grado')->where('asignatura_grados.id', $id)->get();
		$infoestudiante = User::join('estudiantes','users.id', '=', 'estudiantes.user_id')->where('rol','estudiante')->where('grado',$estudiantes[0]['grado'])->orderby('apellidos','ASC')->get();
		return view('docente.subirnotas', ['estudiantes' => $infoestudiante, 'asignatura' => $id]);
	}

/*	public function listarestudiantes($id){
		$grados = AsignaturaGrados::where('id',$id)->first();
		$grado_id = $grados->gradoid;
		$info = AsignaturaGrados::join('calificaciones','asignatura_grados.id','=','calificaciones.asignatura_grados_id')
		->join('estudiantes','calificaciones.estudiante_id','=','estudiantes.id')
		->join('users','estudiantes.user_id','=','users.id')->where('asignatura_grados_id', $id)
		->get();
		if ($info->isempty()){
			$estudiantes = AsignaturaGrados::join('estudiantes', 'asignatura_grados.gradoid', '=', 'estudiantes.grado')->where('asignatura_grados.id', $id)->get();
			$info = User::join('estudiantes','users.id', '=', 'estudiantes.user_id')->where('rol','estudiante')->where('grado',$estudiantes[0]['grado'])->orderby('apellidos','ASC')->get();
			return view('docente.subirnotas', ['estudiantes' => $info, 'asignatura_grados_id' => $id]);
		}

		return view('docente.subirnotas', ['estudiantes' => $info]);
	}*/
#Funcion para Subir Notas
	public function subirnotas(Request $request){
		$periodo1 = $request->input('periodo1');
		$periodo2 = $request->input('periodo2');
		$periodo3 = $request->input('periodo3');
		$estudiante = $request->input('estudiante');
		$asignatura = $request->input('asignatura');

		$isperiodo1 = Calificacion::where('estudiante_id', $estudiante)->where('asignatura_grados_id', $asignatura)->whereNotNull('periodo1')->get();
		$isperiodo2 = Calificacion::where('estudiante_id', $estudiante)->where('asignatura_grados_id', $asignatura)->whereNotNull('periodo2')->get();
		$isperiodo3 = Calificacion::where('estudiante_id', $estudiante)->where('asignatura_grados_id', $asignatura)->whereNotNull('periodo3')->get();

		if($isperiodo1->isempty() && $isperiodo2->isempty() && $isperiodo3->isempty()){
			if (isset($periodo1) && !isset($periodo2) && !isset($periodo3)){
				Calificacion::create([    				
					'asignatura_grados_id' => $asignatura,
					'estudiante_id' => $estudiante,
					'Periodo1' => $periodo1
				]);
			}
			elseif (isset($periodo1) && isset($periodo2) && !isset($periodo3)){
				Calificacion::create([
					'asignatura_grados_id' => $asignatura,
					'estudiante_id' => $estudiante,
					'Periodo1' => $periodo1,
					'Periodo2' => $periodo2
				]);
			}
			elseif (isset($periodo1) && isset($periodo2) && isset($periodo3)){
				Calificacion::create([
					'asignatura_grados_id' => $asignatura,
					'estudiante_id' => $estudiante,
					'Periodo1' => $periodo1,
					'Periodo2' => $periodo2,
					'Periodo3' => $periodo3
				]);
			}
			elseif (!isset($periodo1) && isset($periodo2) && !isset($periodo3)){
				Calificacion::create([
					'asignatura_grados_id' => $asignatura,
					'estudiante_id' => $estudiante,
					'Periodo2' => $periodo2
				]);
			}
			elseif (!isset($periodo1) && !isset($periodo2) && isset($periodo3)){
				Calificacion::create([
					'asignatura_grados_id' => $asignatura,
					'estudiante_id' => $estudiante,
					'Periodo3' => $periodo3
				]);
			}
		}
		else{
			$update = Calificacion::where('estudiante_id', $estudiante)->where('asignatura_grados_id', $asignatura);
			if (($periodo1!=null) && ($periodo2==null) && ($periodo3==null)){
				$update->update([    				
					'Periodo1' => $periodo1
				]);
			}
			elseif (($periodo1!=null) && ($periodo2!=null) && ($periodo3==null)){
				$update->update([
					'Periodo1' => $periodo1,
					'Periodo2' => $periodo2
				]);
			}
			elseif (($periodo1!=null) && ($periodo2!=null) && ($periodo3!=null)){
				$update->update([
					'Periodo1' => $periodo1,
					'Periodo2' => $periodo2,
					'Periodo3' => $periodo3
				]);
			}
			elseif (($periodo1==null) && ($periodo2!=null) && ($periodo3==null)){
				$update->update([
					'Periodo2' => $periodo2
				]);
			}
			elseif (($periodo1==null) && ($periodo2==null) && ($periodo3!=null)){
				$update->update([
					'Periodo3' => $periodo3
				]);
			}
		}
		return redirect('listarestudiantes/'.$asignatura);
	}
}