<?php namespace SchoolAdmin\Http\Controllers\estudiante;

use SchoolAdmin\Http\Requests;
use SchoolAdmin\Http\Controllers\Controller;
use SchoolAdmin\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use SchoolAdmin\Calificacion;
use SchoolAdmin\AsignaturaGrados;

class EstudianteController extends Controller {
	protected $auth;

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}
#Funcion para listar Estudiante
	function getestudiantes(){
		$docentes = User::orderBy('estado','DESC')->where('rol','estudiante')->get();
		return view('estudiante.estudiante',compact('estudiantes'));
	}
#Funcion para ver Notas
	function vernotas(){
		$id = $this->auth->user()->id;
		$estudiante = User::findorfail($id);
		$identificacion = $this->auth->user()->identificacion;
		$estudiantes = User::join('estudiantes','users.id', '=', 'estudiantes.user_id')->where('rol','estudiante')->where('identificacion',$identificacion)->first();
		$estudiante_grado = $estudiantes->grado;
		$estudiante_id = $estudiantes->id;
		$notas = Calificacion::join('asignatura_grados','calificaciones.asignatura_grados_id','=', 'asignatura_grados.id')->join('asignaturas', 'asignatura_grados.asignatura_id', '=', 'asignaturas.id')->get();
		$nota = $notas->where('estudiante_id', $estudiante_id);
		if ($nota->isempty())
		{
			$nota = AsignaturaGrados::join('asignaturas', 'asignatura_grados.asignatura_id', '=', 'asignaturas.id')->where('gradoid', $estudiante_grado)->get();	
		}
		return view('estudiante.vernotas', ['notas' => $nota, 'estudiantes' => $estudiantes]);
	}
}