<?php namespace SchoolAdmin\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class DocenteMiddleware {

	protected $auth;

	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	public function handle($request, Closure $next)
	{
		if ($this->auth->guest())
		{
			if ($request->ajax())
			{
				return response('Unauthorized.', 401);
			}
			else
			{
				return redirect()->guest('/ingresar');
			}
		}
		

		if ($request->user()->rol != 'docente')
        {
            return redirect('/');
        }

		return $next($request);
	}

}
