<?php namespace SchoolAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\hasOne;

class Estudiante extends Model {

	protected $table = 'estudiantes';

	protected $fillable = ['user_id','grado'];

	public function usuarios(){
		return $this->hasOne('SchoolAdmin\User');
	}
}
