<?php namespace SchoolAdmin;

use Illuminate\Database\Eloquent\Model;

class Docente extends Model {

	protected $table = 'docentes';

	protected $fillable = ['user_id'];

}
