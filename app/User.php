<?php namespace SchoolAdmin;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Relations\hasOne;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['nombres', 'apellidos', 'fnacimiento', 'identificacion', 'rol', 'estado', 'email', 'password', 'sexo'];


	protected $hidden = ['password', 'remember_token'];

	public function estudiantes(){
		return $this->hasOne('SchoolAdmin\Estudiante');
	}

}
