<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixingColumnName extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('calificaciones');
		Schema::create('calificaciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('asignatura_grados_id');
			$table->foreign('asignatura_grados_id')->references('id')->on('asignatura_grados')->onDelete('cascade');
			$table->unsignedInteger('estudiante_id');
			$table->foreign('estudiante_id')->references('id')->on('estudiantes')->onDelete('cascade');
			$table->decimal('Periodo1',2,1);
			$table->decimal('Periodo2',2,1);
			$table->decimal('Periodo3',2,1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::create('calificaciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('asignatura_id');
			$table->foreign('asignatura_id')->references('id')->on('asignatura_grados')->onDelete('cascade');
			$table->unsignedInteger('estudiante_id');
			$table->foreign('estudiante_id')->references('id')->on('estudiantes')->onDelete('cascade');
			$table->decimal('Periodo1',2,1);
			$table->decimal('Periodo2',2,1);
			$table->decimal('Periodo3',2,1);
		});
	}

}
