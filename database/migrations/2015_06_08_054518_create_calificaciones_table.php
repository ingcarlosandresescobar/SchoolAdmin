<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalificacionesTable extends Migration {

	public function up()
	{
		Schema::create('calificaciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('asignatura_id');
			$table->foreign('asignatura_id')->references('id')->on('asignatura_grados')->onDelete('cascade');
			$table->unsignedInteger('estudiante_id');
			$table->foreign('estudiante_id')->references('id')->on('estudiantes')->onDelete('cascade');
			$table->decimal('Periodo1',2,1)->nullable();
			$table->decimal('Periodo2',2,1)->nullable();
			$table->decimal('Periodo3',2,1)->nullable();
		});
	}

	public function down()
	{
		Schema::drop('calificaciones');
	}
}
