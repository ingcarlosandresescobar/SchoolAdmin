<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsignaturaGradosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('asignatura_grados', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedInteger('asignatura_id');
			$table->foreign('asignatura_id')->references('id')->on('asignaturas')->onDelete('cascade');
			$table->unsignedInteger('docente_id');
			$table->foreign('docente_id')->references('id')->on('docentes')->onDelete('cascade');
			$table->enum('gradoid', array('1','2','3','4','5','6','7','8','9','10','11'));			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('asignatura_grados');
	}

}
