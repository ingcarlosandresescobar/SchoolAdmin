<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nombres');
			$table->string('apellidos');
			$table->unsignedInteger('identificacion')->unique();
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->string('fnacimiento');
			$table->enum('sexo', ['masculino','femenino']);
			$table->enum('rol', array('estudiante','docente','admin'));
			$table->boolean('estado')->default(true);
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
