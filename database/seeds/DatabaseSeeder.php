<?php
use SchoolAdmin\User;
use SchoolAdmin\Asignatura;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UserTableSeeder');
		$this->command->info('Usuario administrador agregado correctamente!');
	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create([
        	'nombres' 	=> 'Maria Teresa', 
        	'apellidos' => 'Herrera',
        	'identificacion' => '100',
        	'email' 	=> 'mate3028@gmail.com',
        	'password'  => bcrypt('mteresa1'),
        	'fnacimiento' => '1995-10-23',
            'sexo' => 'femenino',
        	'rol' => 'admin',
        	'estado' => 1]);
    }
}
